# Reconfiguring a Mailman3 installation from using an SQLite database to using a PostGreSQL database

## This was my setup. YMMV

I had separate databases, one for Mailman Core, and one for Mailman Web (Django + Postorius + Hyperkitty).
I used [pgloader](https://pgloader.io/) ([GitHub](https://github.com/dimitri/pgloader), [Docs](https://pgloader.readthedocs.io/)) to perform the conversion.

## Mailman Core

I had no `[database]` section in my `mailman.cfg`. This was the default setup.

1. Create a temporary pgsql database (`$PGTMP` below)
2. Change working directory to Mailmans data directory (that's where the SQLite DB is located)
3. Run `pgloader mailman.db postgresql:///$PGTMP`.

The SQLite database is now loaded into pgsql but is not compatible with Mailman.
To fix this, we need to create another pgsql database and point Mailman to it.
Then start and stop Mailman so the database schema is applied, and basic, empty tables are created.

4. Create the final database (`$MAILMANDB` below)
5. Edit `mailman.cfg` and add a `[database]` section:
```
[database]
class: mailman.database.postgresql.PostgreSQLDatabase
url: postgres://$PGUSER:$PGPASSWORD@localhost/$MAILMANDB
```
Of course you need to replace variables as appropriate in the `url:` line above.

6. Start and stop Mailman Core. `$MAILMANDB` should now be filled with tables.

Now we can dump the data from `$PGTMP`, and load it into $MAILMANDB.
But there will be a problem with the `mailinglist` table.
Three of the columns, holding date intervals, will not load into $MAILMANDB because they contain timestamp values, not intervals.
So we need to convert them.

7. Run [this script](mailinglist_fix.sql) on `$PGTMP`. It will replace the broken columns and convert the data.
8. Now dump the data using this command: `pg_dump -a -d $PGTMP -f mailmandb.sql --disable-triggers`
9. Before loading the data, we need to remove all statements regarding the table `alembic_version`, which will already be present in the database, so we don't need to load it.
   Open `mailmandb.sql` in a text editor and remove these statements (at least the COPY statement).
10. Load the data into `$MAILMANDB` using psql: `psql -f mailmandb.sql $MAILMANDB`
When running this command, make sure you use a pgsql user that has system access (e.g. `postgres`).  
If you don't do that, you'll run into errors like this:  
 `ERROR:  permission denied: "RI_ConstraintTrigger_c_40318" is a system trigger`
11. Now we need to fix the sequences. The database restore does not align the sequences with the table contents, so run [this script](mailman-fix-sequences.sql) on `$MAILMANDB`:  
 `psql -f mailman-fix-sequences.sql $MAILMANDB` 

You should now have a working pgsql database. You can start Mailman Core.


## Mailman Web

I was using the [mailmansuite](https://gitlab.com/mailman/mailman-suite) setup, so my Django settings file had a basic sqlite3 database setting like this:
```
DATABASES = {
    'default': {
        # Use 'sqlite3', 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'ENGINE': 'django.db.backends.sqlite3',
        # DB name or path to database file if using sqlite3.
        'NAME': os.path.join(BASE_DIR, 'mailmansuite.db'),
        # The following settings are not used with sqlite3:
        'USER': 'mailmansuite',
        'PASSWORD': 'mmpass',
        # HOST: empty for localhost through domain sockets or '127.0.0.1' for
        # localhost through TCP.
        'HOST': '',
        # PORT: set to empty string for default.
        'PORT': '',
    }
}
```
> **Note:** If you have attachments stored in the database, you should consider moving them to filesystem storage instead.
> Will make the database conversion much faster.

1. Create the target postgres database, example: `createdb -U postgres mailmansuite`.

2. Run pgloader from the location your mailmansuite database is located.  
My invocation was: `pgloader mailmansuite.db postgresql:///mailmansuite`

You will get several warnings, telling you that index identifiers have been truncated (numbers may vary):  
```
WARNING PostgreSQL warning: identifier "idx_41865_auth_group_permissions_group_id_permission_id_0cd325b0_uniq" will be truncated to "idx_41865_auth_group_permissions_group_id_permission_id_0cd325b"
```
This is probably ok, the indexes are created anyway.
I've not run into any issues yet.  

3. Now we need to fix the sequences. The database restore does not align the sequences with the table contents, so run [this script](mailmanweb-fix-sequences.sql) on `mailmansuite`:  
 `psql -f mailmanweb-fix-sequences.sql mailmansuite` 

4. Now edit your Django settings file (hopefully named `settings_local.py`), and switch to using a postgresql database.
This is done in the DATABASES section, shown above.
You need to (at least) change ENGINE and NAME, probably USER and PASSWORD too.

You can start your webserver now. Postorius and Hyperkitty should (hopefully) work now, using your new pgsql database.

