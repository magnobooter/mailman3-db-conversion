﻿ALTER TABLE mailinglist ADD COLUMN autoresponse_grace_period_interval interval;
ALTER TABLE mailinglist ADD COLUMN bounce_info_stale_after_interval interval;
ALTER TABLE mailinglist ADD COLUMN bounce_you_are_disabled_warnings_interval_interval interval;

UPDATE mailinglist set autoresponse_grace_period_interval=make_interval(0,0,0, CAST( ceil(extract(epoch from autoresponse_grace_period)/86400) AS int ) );
UPDATE mailinglist set bounce_info_stale_after_interval=make_interval(0,0,0, CAST( ceil(extract(epoch from bounce_info_stale_after)/86400) AS int ) );
UPDATE mailinglist set bounce_you_are_disabled_warnings_interval_interval=make_interval(0,0,0, CAST( ceil(extract(epoch from bounce_you_are_disabled_warnings_interval)/86400) AS int ) );

ALTER TABLE mailinglist DROP COLUMN autoresponse_grace_period;
ALTER TABLE mailinglist DROP COLUMN bounce_info_stale_after;
ALTER TABLE mailinglist DROP COLUMN bounce_you_are_disabled_warnings_interval;

ALTER TABLE mailinglist RENAME COLUMN autoresponse_grace_period_interval TO autoresponse_grace_period;
ALTER TABLE mailinglist RENAME COLUMN bounce_info_stale_after_interval TO bounce_info_stale_after;
ALTER TABLE mailinglist RENAME COLUMN bounce_you_are_disabled_warnings_interval_interval TO bounce_you_are_disabled_warnings_interval;
