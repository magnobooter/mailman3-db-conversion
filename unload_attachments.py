import os
import progressbar
from hyperkitty.models.email import Attachment

total = Attachment.objects.count()
print( "Loading %d attachments, please wait..." % total )

bar = progressbar.ProgressBar(max_value=total)
i = 0
for att in Attachment.objects.all():
   i = i + 1
   if att.content is None:
       continue
   bar.update(i)

   path = att._get_folder()
   if not os.path.exists(path):
       os.makedirs(path)
   file = os.path.join(path, str(att.counter))
   with open(file, 'wb') as fp:
       fp.write(bytes(att.content))
   att.content = None
   att.save()

bar.update(i)
print()
